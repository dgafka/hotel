<?php

namespace App\ItOps\Infrastructure;

use App\RoomService\Domain\Room;
use Ecotone\Modelling\Annotation\Repository;
use Ecotone\Modelling\InMemoryEventSourcedRepository;

/**
 * @Repository()
 */
class GlobalInMemoryEventSourcedRepository extends InMemoryEventSourcedRepository
{
    public function __construct(array $eventsPerAggregate = [], array $aggregateTypes = [Room::class])
    {
        parent::__construct($eventsPerAggregate, $aggregateTypes);
    }
}