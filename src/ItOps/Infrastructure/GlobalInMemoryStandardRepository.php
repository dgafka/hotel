<?php

namespace App\ItOps\Infrastructure;

use App\Billing\Domain\BookingBillingInformation;
use App\Customer\Domain\Guest;
use App\ItOps\Domain\Booking;
use Ecotone\Modelling\Annotation\Repository;
use Ecotone\Modelling\InMemoryStandardRepository;

/**
 * @Repository()
 */
class GlobalInMemoryStandardRepository extends InMemoryStandardRepository
{
    public function __construct(array $aggregates = [], array $aggregateTypes = [BookingBillingInformation::class, Guest::class, Booking::class])
    {
        parent::__construct($aggregates, $aggregateTypes);
    }
}