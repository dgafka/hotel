<?php

namespace App\ItOps\Domain;

use App\Billing\Domain\BookingBillingInformation;
use App\Billing\Domain\Event\BillingInformationRegistered;
use App\Customer\Domain\Guest;
use App\ItOps\Domain\Event\BookingWasStarted;
use App\ItOps\Domain\Event\BookingWasSuccessful;
use App\RoomService\Domain\Event\RoomReservationWasMade;
use App\RoomService\Domain\Room;
use Ecotone\Messaging\Conversion\MediaType;
use Ecotone\Modelling\Annotation\Aggregate;
use Ecotone\Modelling\Annotation\AggregateIdentifier;
use Ecotone\Modelling\Annotation\CommandHandler;
use Ecotone\Modelling\Annotation\EventHandler;
use Ecotone\Modelling\Annotation\QueryHandler;
use Ecotone\Modelling\CommandBus;
use Ecotone\Modelling\WithAggregateEvents;

/**
 * @Aggregate()
 */
class Booking
{
    use WithAggregateEvents;

    const BOOK = "booking.book";
    const IS_BOOKING_SUCCESSFUL = "booking.isSuccessful";

    private const IN_PROGRESS = "inProgress";
    private const ROOM_RENT = "roomRent";
    private const CREDIT_CARD_REGISTERED = "creditCardRegistered";
    private const SUCCESSFUL = "successful";

    /**
     * @AggregateIdentifier()
     */
    private string $bookingId;

    private string $currentStatus;

    private array $data;

    private function __construct(string $bookingId, string $currentStatus, array $data)
    {
        $this->bookingId     = $bookingId;
        $this->currentStatus = $currentStatus;
        $this->data          = $data;

        $this->record(new BookingWasStarted($bookingId));
    }

    /**
     * @CommandHandler(Booking::BOOK)
     */
    public static function start(array $data) : self
    {
        return new self($data['bookingId'], self::IN_PROGRESS, $data);
    }

    /**
     * @EventHandler()
     */
    public function onStarted(BookingWasStarted $event, CommandBus $commandBus) : void
    {
        $commandBus->convertAndSend(Guest::REGISTER_GUEST, MediaType::APPLICATION_X_PHP_ARRAY, $this->data);
        $commandBus->convertAndSend(BookingBillingInformation::REGISTER_CREDIT_CARD, MediaType::APPLICATION_X_PHP_ARRAY, $this->data);
        $commandBus->convertAndSend(Room::RESERVE_ROOM, MediaType::APPLICATION_X_PHP_ARRAY, $this->data);
    }

    /**
     * @EventHandler()
     */
    public function whenBillingInformationRegistered(BillingInformationRegistered  $event) : void
    {
        if ($this->currentStatus === self::ROOM_RENT) {
            $this->currentStatus = self::SUCCESSFUL;
        }else {
            $this->currentStatus = self::CREDIT_CARD_REGISTERED;

            $this->record(new BookingWasSuccessful($this->bookingId));
        }
    }

    /**
     * @EventHandler()
     */
    public function whenRoomReservationWasMade(RoomReservationWasMade $event) : void
    {
        if ($this->currentStatus === self::CREDIT_CARD_REGISTERED) {
            $this->currentStatus = self::SUCCESSFUL;
        }else {
            $this->currentStatus = self::ROOM_RENT;

            $this->record(new BookingWasSuccessful($this->bookingId));
        }
    }

    /**
     * @QueryHandler(Booking::IS_BOOKING_SUCCESSFUL)
     */
    public function isBookingSuccessful() : bool
    {
        return $this->currentStatus === self::SUCCESSFUL;
    }
}