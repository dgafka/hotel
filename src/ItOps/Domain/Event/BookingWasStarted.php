<?php


namespace App\ItOps\Domain\Event;


class BookingWasStarted
{
    private string $bookingId;

    public function __construct(string $bookingId)
    {
        $this->bookingId = $bookingId;
    }
}