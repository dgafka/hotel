<?php


namespace App\ItOps\Domain\Event;


class BookingWasSuccessful
{
    private string $bookingId;

    public function __construct(string $bookingId)
    {
        $this->bookingId = $bookingId;
    }
}