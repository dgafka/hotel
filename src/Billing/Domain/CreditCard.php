<?php

namespace App\Billing\Domain;

class CreditCard
{
    private string $number;

    private string $validTill;

    private string $cvv;

    public function __construct(string $number, string $validTill, string $cvv)
    {
        $this->number    = $number;
        $this->validTill = $validTill;
        $this->cvv       = $cvv;
    }
}