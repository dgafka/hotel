<?php

namespace App\Billing\Domain\Command;

use App\Billing\Domain\CreditCard;

class RegisterCreditCardForBooking
{
    public string $bookingId;
    public CreditCard $creditCard;
}