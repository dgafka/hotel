<?php


namespace App\Billing\Domain\Event;


class BillingInformationRegistered
{
    private string $bookingId;

    public function __construct(string $bookingId)
    {
        $this->bookingId = $bookingId;
    }
}