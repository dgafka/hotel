<?php

namespace App\Billing\Domain;

use App\Billing\Domain\Command\RegisterCreditCardForBooking;
use App\Billing\Domain\Event\BillingInformationRegistered;
use Ecotone\Modelling\Annotation\Aggregate;
use Ecotone\Modelling\Annotation\AggregateIdentifier;
use Ecotone\Modelling\Annotation\CommandHandler;
use Ecotone\Modelling\WithAggregateEvents;

/**
 * @Aggregate()
 */
class BookingBillingInformation
{
    const REGISTER_CREDIT_CARD = "billing.registerCreditCard";

    use WithAggregateEvents;

    /**
     * @AggregateIdentifier()
     */
    private string $bookingId;

    private CreditCard $creditCard;

    private function __construct(string $bookingId, CreditCard  $creditCard)
    {
        $this->bookingId = $bookingId;
        $this->creditCard = $creditCard;

        $this->record(new BillingInformationRegistered($bookingId));
    }

    /**
     * @CommandHandler(BookingBillingInformation::REGISTER_CREDIT_CARD)
     */
    public static function register(RegisterCreditCardForBooking $command) : self
    {
        return new self($command->bookingId, $command->creditCard);
    }
}