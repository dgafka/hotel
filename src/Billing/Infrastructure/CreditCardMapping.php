<?php


namespace App\Billing\Infrastructure;


use App\Billing\Domain\CreditCard;
use Ecotone\Messaging\Annotation\Converter;

class CreditCardMapping
{
    /**
     * @Converter()
     */
    public function from(array $data) : CreditCard
    {
        return new CreditCard($data['number'], $data['validTill'], $data['cvv']);
    }
}