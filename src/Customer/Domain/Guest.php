<?php


namespace App\Customer\Domain;


use App\Customer\Domain\Command\RegisterGuest;
use Ecotone\Modelling\Annotation\Aggregate;
use Ecotone\Modelling\Annotation\CommandHandler;

/**
 * @Aggregate()
 */
class Guest
{
    const REGISTER_GUEST = "customer.registerGuest";

    private string $bookingId;
    private string $guestName;
    private string $guestSurname;

    private function __construct(string $bookingId, string $guestName, string $guestSurname)
    {
        $this->bookingId = $bookingId;
        $this->guestName    = $guestName;
        $this->guestSurname = $guestSurname;
    }

    /**
     * @CommandHandler(Guest::REGISTER_GUEST)
     */
    public static function register(RegisterGuest $command) : self
    {
        return new self($command->getBookingId(), $command->getGuestName(), $command->getGuestSurname());
    }
}