<?php


namespace App\Customer\Domain\Command;


class RegisterGuest
{
    private string $bookingId;
    private string $guestName;
    private string $guestSurname;

    public function getBookingId(): string
    {
        return $this->bookingId;
    }

    public function getGuestName(): string
    {
        return $this->guestName;
    }

    public function getGuestSurname(): string
    {
        return $this->guestSurname;
    }
}