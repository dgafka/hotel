<?php

namespace App\RoomService\Domain\Command;

use App\RoomService\Domain\ReservationPeriod;

class RentRoom
{
    public string $bookingId;
    public string $roomType;
    public ReservationPeriod $period;
}