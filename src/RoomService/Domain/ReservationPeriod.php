<?php

namespace App\RoomService\Domain;

class ReservationPeriod
{
    private \DateTimeImmutable $from;
    private \DateTimeImmutable $to;

    public function __construct(\DateTimeImmutable $from, \DateTimeImmutable $to)
    {
        $this->from = $from;
        $this->to   = $to;
    }

    public function isIntersecting(ReservationPeriod $period) : bool
    {
        return
            $this->from >= $period->from && $this->from <= $period->from
            ||
            $this->to >= $period->to && $this->to <= $period->to;
    }
}