<?php

namespace App\RoomService\Domain\Event;

use App\RoomService\Domain\ReservationPeriod;

class RoomReservationWasMade
{
    public string $roomType;
    public string $bookingId;
    public ReservationPeriod $reservationPeriod;

    public function __construct(string $roomType, string $bookingId, ReservationPeriod $reservationPeriod)
    {
        $this->roomType = $roomType;
        $this->bookingId         = $bookingId;
        $this->reservationPeriod = $reservationPeriod;
    }
}