<?php

namespace App\RoomService\Domain;

use App\RoomService\Domain\Event\RoomReservationWasMade;
use Ecotone\Modelling\Annotation\EventHandler;
use Ecotone\Modelling\Annotation\QueryHandler;

class AvailabilityCalendar
{
    const GET_AVAILABLE_ROOMS = "roomService.isAvailable";

    private array $calendar = [
        Room::CITY_VIEW => [],
        Room::PREMIUM_VIEW => [],
        Room::BRIDGE_VIEW => []
    ];

    /**
     * @QueryHandler(AvailabilityCalendar::GET_AVAILABLE_ROOMS)
     */
    public function getAvailable(ReservationPeriod $selectedReservationDate) : array
    {
        $availableRooms = [];
        foreach ($this->calendar as $roomType => $roomReservedPeriods) {
            if ($this->isAvailable($roomReservedPeriods, $selectedReservationDate)) {
                $availableRooms[] = $roomType;
            }
        }

        return $availableRooms;
    }

    /**
     * @EventHandler()
     */
    public function adjustCalendar(RoomReservationWasMade $event) : void
    {
        $this->calendar[$event->roomType][] = $event->reservationPeriod;
    }

    /**
     * @var ReservationPeriod[] $roomReservedPeriods
     */
    private function isAvailable(array $roomReservedPeriods, ReservationPeriod $selectedReservationDate): bool
    {
        foreach ($roomReservedPeriods as $roomReservedPeriod) {
            if ($roomReservedPeriod->isIntersecting($selectedReservationDate)) {
                return false;
            }
        }

        return true;
    }
}