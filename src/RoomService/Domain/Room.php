<?php

namespace App\RoomService\Domain;

use App\RoomService\Domain\Command\RentRoom;
use App\RoomService\Domain\Event\RoomReservationWasMade;
use Ecotone\Modelling\Annotation\Aggregate;
use Ecotone\Modelling\Annotation\AggregateFactory;
use Ecotone\Modelling\Annotation\AggregateIdentifier;
use Ecotone\Modelling\Annotation\CommandHandler;
use Ecotone\Modelling\WithAggregateEvents;

/**
 * @Aggregate()
 */
class Room
{
    const RESERVE_ROOM = "roomService.reserve";

    const CITY_VIEW = "cityView";
    const PREMIUM_VIEW = "premiumView";
    const BRIDGE_VIEW = "bridgeView";

    /**
     * @AggregateIdentifier()
     */
    private string $roomType;

    use WithAggregateEvents;

    private function __construct() {}

    /**
     * @CommandHandler(Room::RESERVE_ROOM)
     */
    public static function reserveFirstTime(RentRoom $command) : array
    {
        return [new RoomReservationWasMade($command->roomType, $command->bookingId, $command->period)];
    }

    /**
     * @CommandHandler(Room::RESERVE_ROOM)
     */
    public function reserve(RentRoom $command) : array
    {
        return [new RoomReservationWasMade($command->roomType, $command->bookingId, $command->period)];
    }

    /**
     * @AggregateFactory()
     */
    public static function apply(array $events) : self
    {
        $self = new self();
        foreach ($events as $event) {
            switch (get_class($event)) {
                case RoomReservationWasMade::class: {
                    $self->roomType = $event->roomType;
                    break;
                }
            }
        }

        return $self;
    }
}