<?php


namespace App\RoomService\Infrastructure;

use App\RoomService\Domain\ReservationPeriod;
use Ecotone\Messaging\Annotation\Converter;

class ReservationDateMapping
{
    /**
     * @Converter()
     */
    public function to(array $data) : ReservationPeriod
    {
        return new ReservationPeriod(new \DateTimeImmutable($data["from"], new \DateTimeZone("UTC")), new \DateTimeImmutable($data["to"], new \DateTimeZone("UTC")));
    }
}