<?php

namespace App;

use App\ItOps\Domain\Booking;
use App\RoomService\Domain\AvailabilityCalendar;
use App\RoomService\Domain\Room;
use Ecotone\Messaging\Conversion\MediaType;
use Ecotone\Modelling\CommandBus;
use Ecotone\Modelling\QueryBus;
use PHPUnit\Framework\Assert;

class EcotoneQuickstart
{
    private CommandBus $commandBus;
    private QueryBus $queryBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function run() : void
    {
        $availableRooms = $this->queryBus->convertAndSend(AvailabilityCalendar::GET_AVAILABLE_ROOMS, MediaType::APPLICATION_X_PHP_ARRAY, ["from" => "2010-05-10", "to" => "2010-05-13"]);
        Assert::assertEquals([Room::CITY_VIEW, Room::PREMIUM_VIEW, Room::BRIDGE_VIEW], $availableRooms);

        $bookingId = 1;
        $this->commandBus->convertAndSend(Booking::BOOK, MediaType::APPLICATION_X_PHP_ARRAY, [
            "bookingId" => $bookingId,
            "guestName" => "Adam",
            "guestSurname" => "Frank",
            "roomType" => Room::CITY_VIEW,
            "period" => ["from" => "2010-05-10", "to" => "2010-05-13"],
            "creditCard" => [
                "number" => 123,
                "validTill" => "12/20",
                "cvv" => "043"
            ]
        ]);

        $availableRooms = $this->queryBus->convertAndSend(AvailabilityCalendar::GET_AVAILABLE_ROOMS, MediaType::APPLICATION_X_PHP_ARRAY, ["from" => "2010-05-10", "to" => "2010-05-13"]);
        Assert::assertEquals([Room::PREMIUM_VIEW, Room::BRIDGE_VIEW], $availableRooms, "Room was not rented correctly");


        Assert::assertTrue($this->queryBus->convertAndSend(Booking::IS_BOOKING_SUCCESSFUL, MediaType::APPLICATION_X_PHP_ARRAY, ["bookingId" => $bookingId]), "Booking was not successful");
    }
}