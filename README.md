Home work:
-

- Identify Service Boundaries?
- What data does each service own?
- What part of which UI does it own?
- What events does it publish / subscribe to?

Use Cases:
- 

- For a single hotel 
- With only 1 guest per reservation
- With 1 room per reservation
- No loyalty program / sign-in
- Rooms are not locked while booking in process
- Capacity must be respected. Can't overbook the hotel

Use Cases (v1): Searching
---

Search for availability. User comes in choose from and to date and gets possible rooms
Example result:

  - City View Guest Room, cost 120E
  - Premium City View Guest Room, cost 160E
  - Bridge View Guest Room, cost 200E

Use Cases (v2.1): Booking
---
 
Given there is availability, user can do a booking
   - There may be a different price for each day
   - User gets summary price for whole period
   - User provides guest info
   - User provides credit card information
   - and do the actual booking
   
   
Use Cases (v2.2): Authorization for cancellation fee
---

If user did not cancel booking 24 hours before it starts we may charge him
with cancellation fee

- before making booking, authorize credit card for cancellation fee
- If successful, see if a room is still available
  If not, release authorization, tell user "no room"
  
Use Cases (v3): Front Desk
---  

- Check in booked user by last name
- Verify info
- Authorize credit card for full stay
- Allocate room

Use Cases (v4): Front Desk
---  

- Checkout out
- night  before print out the bill
- Charge for amount used in minibar or if something destroyed